<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid\Field;

use WenRuns\Laravel\Admin\Form\Field\Grid\Column;

class Email extends Column
{
    protected $type = 'email';


    public function verify($verify = true)
    {
        return $this->config('verify', $verify);
    }


}
