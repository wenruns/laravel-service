laravel-admin extension
======

## 1、安装

```
composer require wenruns/laravel-service
```

## 2、发布静态资源

```angular2html
php artisan vendor:publish --provider=WenRuns\Laravel\LaravelServiceProvider
```

## 3、功能介绍

### (1) GridService服务

```php
该服务二次封装的Grid的使用，美化代码视图。例如：
$grid = GridService::instance(ModelClass)
    ->where(function(Grid/Model $model) {

    })
    ->content(function(Grid $grid){
    
    })
    ->tools(function(Tools $tools){
    
    })
    ->actions(function(Actions $actions){
    
    })
    ->statistic(['consume','number']) // 统计当页指定字段
    ->mergeColspan(["id"]) // 合并相同的值
    ->exporter(new Exporter::class)
    ->disableCreateButton()
    ->disableFilter()
    ->disableBatchActions()
    ->render()
```

### (2) FormService服务

```php
该服务二次封装Form表单的使用，美化代码试图。例如：
$form = FormService::instance(Model::class)
    ->content(function(Form $form){
    
    })
    ->saving(Form $form){
    
    }
    ->render();
```

###（3）其他小功能
