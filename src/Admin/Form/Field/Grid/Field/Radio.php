<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid\Field;

use WenRuns\Laravel\Admin\Form\Field\Grid\Column;

class Radio extends Column
{
    protected $type = 'radio';


    public function options($options)
    {
        return $this->config('options', $options);
    }
}
