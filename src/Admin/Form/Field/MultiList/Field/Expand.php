<?php
/**
 * Created by PhpStorm.
 * User: Administrator【wenruns】
 * Date: 2021/1/26
 * Time: 10:41
 */

namespace WenRuns\Laravel\Admin\Form\Field\MultiList\Field;


use Encore\Admin\Facades\Admin;
use WenRuns\Laravel\Admin\Form\Field\MultiList\Field;
use WenRuns\Laravel\Admin\Form\Field\MultiList\Row;

class Expand extends Field
{

    protected $disableCheck = true;

    protected $row = null;


    public function build()
    {
        // TODO: Implement build() method.
        $name = $this->withoutName ? '' : $this->getName() . '[value]';
        return <<<HTML
<div class="form-group" style="padding: 0px 0px 0px 15px; width: 100%;{$this->style}">
    <span class="grid-expand-grid-row" data-inserted="0" data-key="{$this->getClass()}" data-toggle="collapse" data-target="#grid-collapse-{$this->getClass()}" aria-expanded="false">
        <a href="javascript:void(0)"><i class="fa fa-angle-double-down"></i>&nbsp;&nbsp;{$this->value()}</a>
        <input type="hidden" id="{$this->getClass()}" class="{$this->getClass()} {$this->getColumnClass()}" name="{$name}" value="{$this->value()}" />
    </span>
</div>
HTML;
    }

    protected function buildEmpty(): string
    {
        $editClass = 'grid-expand-row-edit-' . $this->getUniqueKey();
        $script = <<<SCRIPT
$(function(){
    $("body").on('click', '.{$editClass}', function(e){
        let prevEle = e.currentTarget.previousElementSibling;
        let ele = e.currentTarget.parentElement.querySelector('.{$editClass}-content');
        prevEle.type='text';
        prevEle.value = ele.innerHTML;
        prevEle.focus();
    });
    $("body").on('keydown', ".{$editClass}-input", function(e){
        let event = e || window.event;
        if(e.keyCode == 13){
            event.preventDefault();
            let ele = e.currentTarget.parentElement.querySelector('.{$editClass}-content');
            ele.innerHTML = e.currentTarget.value;
            e.currentTarget.type = 'hidden';
        }
    });
    $("body").on('blur', ".{$editClass}-input", function(e){
        let ele = e.currentTarget.parentElement.querySelector('.{$editClass}-content');
        ele.innerHTML = e.currentTarget.value;
        e.currentTarget.type = 'hidden';
    });
});
SCRIPT;
        Admin::script($script);
        $name = $this->withoutName ? '' : $this->getName() . '[value]';
        return <<<HTML
<div class="form-group" style="padding: 0px 0px 0px 15px; width: 100%;{$this->style}; position:relative;">
    <span class="grid-expand-grid-row" data-inserted="0" data-key="{$this->getClass()}" data-toggle="collapse" data-target="#grid-collapse-{$this->getClass()}" aria-expanded="false">
        <a href="javascript:void(0)"><i class="fa fa-angle-double-down"></i>&nbsp;&nbsp;<span class="{$editClass}-content">{$this->value()}</span></a>
    </span>
    <input style="position: absolute; top:0px; min-width: 100px;width: 100%;" type="hidden" id="{$this->getClass()}" name="{$name}" value="{$this->value()}" placeholder="{$this->getPlaceholder()}" class="form-control text {$this->getClass()} {$this->getColumnClass()} {$editClass}-input" />
    <i class="fa fa-edit {$editClass}" data-expandEdit="true" style="cursor:pointer;"></i>
</div>
HTML;
    }

    protected function tdStart()
    {
        $colspan = $this->columnInstance->getMultiList()->getTableList()->getColumnLen();
        return '<td colspan="' . $colspan . '" class="' . $this->getClass() . '" style="padding:0 !important; border:0;">';
    }

    /**
     * @return string
     */
    protected function tdEnd()
    {
        return '</td>';
    }

    protected function trStart($isSub = false)
    {
        if ($isSub) {
            return '<tr data-sub="true">';
        }
        return '<tr>';
    }

    protected function trEnd()
    {
        return '</tr>';
    }


    public function renderSub()
    {
        $expandContent = '<div class="collapse" id="grid-collapse-' . $this->getClass() . '" aria-expanded="true"><div style="padding: 10px 10px 0px 10px !important;">';
        if ($this->callback) {
            $this->row = new Row(
                $this->columnInstance->getMultiList(),
                $this->columnInstance->getTableList(),
                $this->column,
                $this->label,
                'text',
                $this->withoutName ? '' : $this->getName() . '[children]',
                $this->callback,
                $this
            );
            $this->row->subItems($this->columnInstance->subItems)
                ->keyIsVariable($this->keyIsVariable)
                ->createEmpty($this->createEmpty);
            $this->callback->call($this, $this->row);
            $expandContent .= $this->row->render();
        }
        $expandContent .= '</div></div>';
        $this->oneRowScript = $this->row->getOneRowScript();
        $this->script = $this->row->getScript();
        return $this->trStart(true) . $this->tdStart() . $expandContent . $this->tdEnd() . $this->trEnd();
    }

}
