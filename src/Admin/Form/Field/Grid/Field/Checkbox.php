<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid\Field;

use WenRuns\Laravel\Admin\Form\Field\Grid\Column;

class Checkbox extends Column
{
    protected $type = 'checkbox';


    public function options($options)
    {
        return $this->config('options', $options);
    }

}
