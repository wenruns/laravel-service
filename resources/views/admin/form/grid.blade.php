<div class="form-grid {{$viewClass['form-group']}} {!! !$errors->has($errorKey) ? '' : 'has-error' !!}">
    <label for="{{$id}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>
    <div class="{{$viewClass['field']}} box-body" style="padding: 0px 10px 5px; overflow-x: auto;">
        @include('admin::form.error')
        <!---grid_start--->
        {!! $__content__ !!}
        <!---grid_end--->
        @include('admin::form.help-block')
    </div>
</div>
