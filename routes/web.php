<?php

use WenRuns\Laravel\Http\Controllers\LaravelController;
use WenRuns\Laravel\Config;

// 插件介绍
Route::get('wenruns/laravel-service', LaravelController::class . '@index');

// 异步获取Modal、Expand
Route::get('__form_grid__', LaravelController::class . '@formGrid')->name('wenruns.laravel.service.form.grid');
// 图片上传
Route::post('__form_grid_upload__', LaravelController::class . '@formGridUpload')->name('wenruns.laravel.service.form.grid.upload');


