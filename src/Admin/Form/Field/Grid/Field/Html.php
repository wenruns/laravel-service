<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid\Field;

use WenRuns\Laravel\Admin\Form\Field\Grid\Column;
use WenRuns\Laravel\Admin\Form\Field\Grid\Grid;

class Html extends Column
{
    /**
     * @var string
     */
    protected $type = 'html';

    /**
     * @var string
     */
    protected $html;

    public function __construct(Grid $grid, $label, $html)
    {
        $this->html = $html;
        parent::__construct($grid, null, $label);
    }

    /**
     * @return array
     */
    public function render()
    {
        return array_merge(parent::render(), [
            'label' => $this->label,
            'name' => $this->column,
            'type' => $this->type,
            'html' => $this->html,
        ]);
    }

    /**
     * @return null
     */
    public function getValue()
    {
        if (is_callable($this->withClosure)) {
            $this->html = call_user_func($this->withClosure, $this->html);
        }
        return null;
    }

}
