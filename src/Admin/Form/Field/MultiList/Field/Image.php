<?php


namespace WenRuns\Laravel\Admin\Form\Field\MultiList\Field;


use WenRuns\Laravel\Admin\Form\Field\MultiList\Field;

class Image extends Field
{

    protected $multiple = true;


    public function disableMultiple($multiple = false)
    {
        $this->multiple = $multiple;
        return $this;
    }

    protected function get_script()
    {
        return <<<SCRIPT
$(".{$this->getColumnClass()}").on('change','input[type="file"][data-upload]',function(e){
    //建立可存取到该file的url
    function getObjectURL(file) {
        var url = null;
        //下面函数执行效果是一样的，只是针对不同的浏览器执行不同的js函数而已
        if (window.createObjectURL != undefined) { //basic
            url = window.createObjectURL(file);
        } else if (window.URL != undefined) { //mozilla(firefox)
            url = window.URL.createObjectURL(file);
        } else if (window.webkitURL != undefined) { //webkit or chrome
            url = window.webkitURL.createObjectURL(file);
        }
        return url;
    }
    function changeImg(obj) {
        var prev = obj.parentElement.parentElement;
        var btn = obj.parentElement;
        if(btn.classList.value != 'upload-button'){
            return ;
        }
        var flag = Math.round(Math.random()*100000)+Date.now();
        prev.querySelectorAll(".preview-img").forEach(el=>{
            el.remove();
        });
        Array.from(obj.files).forEach((file, i)=>{
            var imgSize = file.size;
            if (file.type.indexOf('image/')<0) {
                return alert("上传图片格式不正确");
            };
            var imgSrc = getObjectURL(file);
            var prevImg = document.createElement("div");
            prevImg.classList.add('preview-img');
            prevImg.style = 'width: 34px;height: 34px;position:relative;overflow:hidden;display: flex;justify-content: center;';
            var img = document.createElement("img");
            img.classList.add("img");
            img.classList.add("img-thumbnail");
            img.style = "max-width: 100%;max-height: 100%;";
            img.src = imgSrc;
            img.setAttribute("layer-src", imgSrc);
            prevImg.innerHTML += img.outerHTML;
            if(i == 0){
                var input = document.createElement("input");
                Array.from(obj.attributes).forEach(item=>{
                    input.setAttribute(item.name, item.value);
                });
                obj.parentElement.insertBefore(input, obj);
                prevImg.append(obj);
                obj.removeAttribute("data-upload");
                obj.style.width = '0px';
                obj.style.height = '0px';
                obj.setAttribute("data-flag", flag);
            }
            prev.insertBefore(prevImg, btn);
        });
        layer.photos({
            photos: prev,
            tab: function(pic, layero){
                let el = layero[0];
                if(el){
                    let closeBtn = el.querySelector(".close-prev-image");
                    if(!closeBtn){
                        closeBtn = document.createElement("a");
                        closeBtn.classList.add("btn");
                        closeBtn.classList.add("btn-danger");
                        closeBtn.classList.add("btn-xs");
                        closeBtn.innerHTML = "+";
                        closeBtn.style = 'position: absolute;'
                            +'top: -10px;'
                            +'right: -10px;'
                            +'width: 30px;'
                            +'height: 30px;'
                            +'border-radius: 100%;'
                            +'display: flex;'
                            +'justify-content: center;'
                            +'align-items: center;'
                            +'font-size: 25px;'
                            +'transform: rotate(45deg);';
                        el.append(closeBtn);
                    }
                    closeBtn.addEventListener("click", function(){
                        layer.closeAll();
                    });
                }
            },
            //anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        });
    }
    changeImg(e.currentTarget);
});
SCRIPT;
    }

    public function build()
    {
        // TODO: Implement build() method.
        $name = $this->getName();
        $multiple = '';
        if ($this->multiple) {
            $name .= '[]';
            $multiple = 'multiple';
        }
        $value = $this->getValue();
        $preview = '';
        if (is_array($value)) {
            foreach ($value as $val) {
                $preview .= '<div class="preview-img" style="width: 34px;height: 34px;position:relative;overflow:hidden;display: flex;justify-content: center;">'
                    . '<img src="' . $val . '" style="max-width: 100%;max-height: 100%;" layer-src="' . $val . '" class="img img-thumbnail" />'
                    . '<input name="' . $name . '" value="' . $val . '" style="width:0px;height:0px;opacity: 0;filter:opacity(0);position: absolute;" />'
                    . '</div>';
            }
        } else if ($value) {
            $preview = '<div class="preview-img" style="width: 34px;height: 34px;position:relative;overflow:hidden;display: flex;justify-content: center;">'
                . '<img src="' . $value . '" style="max-width: 100%;max-height: 100%;" layer-src="' . $value . '" class="img img-thumbnail" />'
                . '<input name="' . $name . '" value="' . $value . '" style="width:0px;height:0px;opacity: 0;filter:opacity(0);position: absolute;" />'
                . '</div>';
        }
        $this->script = $this->get_script();

        return <<<HTML
<div style="{$this->style}" class="{$this->getColumnClass()} {$this->getClass()}" {$this->buildAttribute()}>
    <span class="{$this->asterisk}"></span>
    <div class="image-box" style="display: flex;flex-wrap: wrap;">
        {$preview}
        <div class="upload-button" style="position: relative;overflow: hidden;">
            <svg t="1656404865959" class="icon" viewBox="0 0 1297 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2675" width="32" height="32"><path d="M759.466667 0c60.3136 0 110.4896 44.066133 113.5616 100.727467l0.170666 5.5296v116.394666a28.672 28.672 0 0 1-57.173333 3.345067l-0.2048-3.345067v-116.394666c0-25.053867-22.254933-46.626133-51.643733-48.7424l-4.676267-0.170667H350.139733c-30.071467 0-53.828267 20.343467-56.149333 44.987733l-0.170667 3.925334v168.106666a28.672 28.672 0 0 1-28.672 28.672l-0.477866-0.034133-0.4096 0.034133H115.234133A57.344 57.344 0 0 0 58.026667 356.113067l-0.136534 4.3008v548.864a57.344 57.344 0 0 0 53.077334 57.207466l4.266666 0.170667h911.7696a57.344 57.344 0 0 0 57.207467-53.077333l0.136533-4.3008V534.528a28.672 28.672 0 0 1 57.173334-3.345067l0.2048 3.345067v374.784c0 61.44-48.298667 111.616-108.987734 114.5856l-5.7344 0.136533H115.234133c-61.44 0-111.616-48.298667-114.5856-108.987733l-0.136533-5.7344V360.448c0-61.44 48.298667-111.616 108.987733-114.5856l5.7344-0.136533 121.207467-0.034134V106.257067c0-57.4464 48.298667-103.3216 107.895467-106.120534l5.802666-0.136533h409.361067z m-188.347734 365.806933c42.2912 0 83.1488 10.205867 119.808 29.457067l3.208534 1.9456a28.672 28.672 0 0 1 9.693866 35.1232 54.545067 54.545067 0 0 0 68.266667 73.352533h-0.034133l3.584-1.024a28.672 28.672 0 0 1 32.324266 16.5888 258.116267 258.116267 0 1 1-236.8512-155.4432z m0 57.344a200.772267 200.772267 0 1 0 193.3312 146.5344l-1.194666-3.9936-1.774934 0.2048-7.714133 0.273067a111.9232 111.9232 0 0 1-111.138133-125.3376l0.648533-4.266667-5.461333-2.048a199.918933 199.918933 0 0 0-56.866134-11.093333z m541.934934-320.853333a28.672 28.672 0 0 1 28.672 28.672v114.688l114.688 0.034133a28.672 28.672 0 0 1 0 57.344h-114.688v114.722134a28.672 28.672 0 0 1-57.344 0V303.035733h-114.756267a28.672 28.672 0 0 1 0-57.344l114.722133-0.034133v-114.688a28.672 28.672 0 0 1 28.672-28.672z" fill="#3586FF" p-id="2676"></path></svg>
            <input type="file" id="{$this->getClass()}" data-upload name="{$name}" {$multiple} style="position: absolute;left: 0px;top: 0px;width: 100%;height: 100%;opacity: 0;filter: opacity(0);">
        </div>
    </div>
    {$this->helpText()}
</div>
HTML;

    }

    protected function buildEmpty(): string
    {
        // TODO: Implement buildEmpty() method.
        $this->oneRowScript = $this->get_script();
        $name = $this->getName();
        $multiple = '';
        if ($this->multiple) {
            $name .= '[]';
            $multiple = 'multiple';
        }
        return <<<HTML
<div style="{$this->style}" class="{$this->getColumnClass()} {$this->getClass()}" {$this->buildAttribute()}>
    <span class="{$this->asterisk}"></span>
    <div class="image-box" style="display: flex;flex-wrap: wrap">
        <div class="upload-button" style="position: relative;overflow: hidden;">
            <svg t="1656404865959" class="icon" viewBox="0 0 1297 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2675" width="32" height="32"><path d="M759.466667 0c60.3136 0 110.4896 44.066133 113.5616 100.727467l0.170666 5.5296v116.394666a28.672 28.672 0 0 1-57.173333 3.345067l-0.2048-3.345067v-116.394666c0-25.053867-22.254933-46.626133-51.643733-48.7424l-4.676267-0.170667H350.139733c-30.071467 0-53.828267 20.343467-56.149333 44.987733l-0.170667 3.925334v168.106666a28.672 28.672 0 0 1-28.672 28.672l-0.477866-0.034133-0.4096 0.034133H115.234133A57.344 57.344 0 0 0 58.026667 356.113067l-0.136534 4.3008v548.864a57.344 57.344 0 0 0 53.077334 57.207466l4.266666 0.170667h911.7696a57.344 57.344 0 0 0 57.207467-53.077333l0.136533-4.3008V534.528a28.672 28.672 0 0 1 57.173334-3.345067l0.2048 3.345067v374.784c0 61.44-48.298667 111.616-108.987734 114.5856l-5.7344 0.136533H115.234133c-61.44 0-111.616-48.298667-114.5856-108.987733l-0.136533-5.7344V360.448c0-61.44 48.298667-111.616 108.987733-114.5856l5.7344-0.136533 121.207467-0.034134V106.257067c0-57.4464 48.298667-103.3216 107.895467-106.120534l5.802666-0.136533h409.361067z m-188.347734 365.806933c42.2912 0 83.1488 10.205867 119.808 29.457067l3.208534 1.9456a28.672 28.672 0 0 1 9.693866 35.1232 54.545067 54.545067 0 0 0 68.266667 73.352533h-0.034133l3.584-1.024a28.672 28.672 0 0 1 32.324266 16.5888 258.116267 258.116267 0 1 1-236.8512-155.4432z m0 57.344a200.772267 200.772267 0 1 0 193.3312 146.5344l-1.194666-3.9936-1.774934 0.2048-7.714133 0.273067a111.9232 111.9232 0 0 1-111.138133-125.3376l0.648533-4.266667-5.461333-2.048a199.918933 199.918933 0 0 0-56.866134-11.093333z m541.934934-320.853333a28.672 28.672 0 0 1 28.672 28.672v114.688l114.688 0.034133a28.672 28.672 0 0 1 0 57.344h-114.688v114.722134a28.672 28.672 0 0 1-57.344 0V303.035733h-114.756267a28.672 28.672 0 0 1 0-57.344l114.722133-0.034133v-114.688a28.672 28.672 0 0 1 28.672-28.672z" fill="#3586FF" p-id="2676"></path></svg>
            <input type="file" id="{$this->getClass()}" data-upload name="{$name}" {$multiple} style="position: absolute;left: 0px;top: 0px;width: 100%;height: 100%;opacity: 0;filter: opacity(0);">
        </div>
    </div>
    {$this->helpText()}
</div>
HTML;
    }


}
