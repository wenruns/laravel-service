<?php
namespace WenRuns\Laravel\Admin\Grid\Column;

use Encore\Admin\Grid\Column\Sorter as GridSorter;

class Sorter extends GridSorter
{

    public $callback = null;

    public function __construct($sortName, $columnName, $cast, $callback = null)
    {
        $this->callback = $callback;
        parent::__construct($sortName, $columnName, $cast);
    }



}