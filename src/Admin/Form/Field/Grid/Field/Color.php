<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid\Field;

use WenRuns\Laravel\Admin\Form\Field\Grid\Column;

class Color extends Column
{
    protected $type = 'color';

    protected function initialization()
    {
        $this->configs([
            'options' => [],
        ]);
        return parent::initialization();
    }


    public function options(array $options)
    {
        $this->configs['options'] = array_merge($this->configs['options'], $options);
        return $this;
    }

    public function option($name, $value)
    {
        $this->configs['options'][$name] = $value;
        return $this;
    }


}
