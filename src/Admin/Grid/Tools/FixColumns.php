<?php

namespace WenRuns\Laravel\Admin\Grid\Tools;

class FixColumns extends \Encore\Admin\Grid\Tools\FixColumns
{
    protected $view = 'WenAdmin::grid.fixed-table';

}
