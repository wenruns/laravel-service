<?php
/**
 * Created by PhpStorm.
 * User: Administrator【wenruns】
 * Date: 2021/1/26
 * Time: 10:41
 */

namespace WenRuns\Laravel\Admin\Form\Field\MultiList\Field;

use Encore\Admin\Facades\Admin;
use WenRuns\Laravel\Admin\Form\Field\MultiList\Field;
use WenRuns\Laravel\Admin\Form\Field\MultiList\Row;

class Modal extends Field
{
    protected $row = null;

    protected $disableCheck = true;

    public function build()
    {
        // TODO: Implement build() method.
        $name = $this->withoutName ? '' : $this->getName() . '[value]';
        return <<<HTML
<div class="form-group" style="padding: 0px 0px 0px 15px;width: 100%;{$this->style}">
    <span class="grid-expand" data-toggle="modal" data-target="#grid-modal-{$this->getClass()}">
        <a href="javascript:void(0)"><i class="fa fa-clone"></i>&nbsp;&nbsp;{$this->value()}</a>
        <input type="hidden" id="{$this->getClass()}" class="{$this->getClass()} {$this->getColumnClass()}" name="{$name}" value="{$this->value()}" />
    </span>
</div>
HTML;
    }


    protected function buildEmpty(): string
    {
        $editClass = 'grid-expand-row-edit-' . $this->getUniqueKey();
        $script = <<<SCRIPT
$(function(){
    $("body").on('click', '.{$editClass}', function(e){
        let prevEle = e.currentTarget.previousElementSibling;
        let ele = e.currentTarget.parentElement.querySelector('.{$editClass}-content');
        prevEle.type='text';
        prevEle.value = ele.innerHTML;
        prevEle.focus();
    });
    $("body").on('keydown', ".{$editClass}-input", function(e){
        let event = e || window.event;
        if(e.keyCode == 13){
            event.preventDefault();
            let ele = e.currentTarget.parentElement.querySelector('.{$editClass}-content');
            ele.innerHTML = e.currentTarget.value;
            e.currentTarget.type = 'hidden';
        }
    });
    $("body").on('blur', ".{$editClass}-input", function(e){
        let ele = e.currentTarget.parentElement.querySelector('.{$editClass}-content');
        ele.innerHTML = e.currentTarget.value;
        e.currentTarget.type = 'hidden';
    });
});
SCRIPT;
        Admin::script($script);
        // TODO: Implement buildEmpty() method.
        $name = $this->withoutName ? '' : $this->getName() . '[value]';
        return <<<HTML
<div class="form-group" style="padding: 0px 0px 0px 15px;width: 100%;{$this->style}; position: relative;">
    <span class="grid-expand" data-toggle="modal" data-target="#grid-modal-{$this->getClass()}">
        <a href="javascript:void(0)"><i class="fa fa-clone"></i>&nbsp;&nbsp;<span class="{$editClass}-content">{$this->value()}</span></a>
    </span>
    <input style="position: absolute; top: 0px;left: 0px; width: 100%; min-width: 100px;" type="hidden" id="{$this->getClass()}" class="form-control text {$editClass}-input {$this->getClass()} {$this->getColumnClass()}" name="{$name}" value="{$this->value()}" />
    <i class="fa fa-edit {$editClass}"></i>
</div>
HTML;
//        return <<<HTML
//<div class="form-group" style="padding: 0px 0px 0px 15px;width: 100%;{$this->style}; display: flex;">
//    <span class="grid-expand" data-toggle="modal" data-target="#grid-modal-{$this->getClass()}">
//        <a href="javascript:void(0)"><i class="fa fa-clone"></i>&nbsp;&nbsp;</a>
//    </span>
//    <input type="text" id="{$this->getClass()}" class="{$this->getClass()} {$this->getColumnClass()}" name="{$this->getName()}"  value="" class="form-control text" placeholder="{$this->getPlaceholder()}" />
//</div>
//HTML;
    }

    protected function tdStart()
    {
        $colspan = $this->columnInstance->getMultiList()->getTableList()->getColumnLen();
        return '<td colspan="' . $colspan . '" class="' . $this->getClass() . '" style="padding:0 !important; border:0;">';
    }

    protected function tdEnd()
    {
        return '</td>';
    }

    protected function trStart($isSub = false)
    {
        if ($isSub) {
            return '<tr data-sub="true">';
        }
        return '<tr>';
    }

    protected function trEnd()
    {
        return '</tr>';
    }

    public function renderSub()
    {
        $expandContent = '';
        if ($this->callback) {
            $this->row = new Row(
                $this->columnInstance->getMultiList(),
                $this->columnInstance->getTableList(),
                $this->column,
                $this->label,
                'text',
                $this->withoutName ? '' : $this->getName() . '[children]',
                $this->callback,
                $this
            );
            $this->row
                ->subItems($this->columnInstance->subItems)
                ->keyIsVariable($this->keyIsVariable)
                ->createEmpty($this->createEmpty);

            $this->callback->call($this, $this->row);
            $expandContent .= $this->row->render();
        }
        $title = '--' . $this->value();
        if ($this->createEmpty) {
            $title = '';
        }
        $this->oneRowScript = $this->row->getOneRowScript();
        $this->script = $this->row->getScript();
        return <<<HTML
{$this->trStart(true)}
{$this->tdStart()}
<div class="modal" id="grid-modal-{$this->getClass()}" tabindex="-1" role="dialog" aria-hidden="true">
    <div style="width: 100%;height: 100%;display: flex;justify-content: center;align-items: center;">
        <div class="modal-content" style="min-width: 50%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">{$this->getPlaceholder()}<span class="{$this->getClass()} modal-title-text">{$title}</span></h4>
            </div>
            <div class="modal-body">{$expandContent}</div>
        </div>
    </div>
</div>
{$this->tdEnd()}
{$this->trEnd()}
HTML;
    }

}
