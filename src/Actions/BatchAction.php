<?php

namespace WenRuns\Laravel\Actions;

use Encore\Admin\Facades\Admin;

abstract class BatchAction extends \Encore\Admin\Actions\BatchAction
{
    protected $permission = null;


    public function __construct($permission = null)
    {
        if ($permission) {
            $this->permission = $permission;
        }
        parent::__construct();
    }


    public function hasPermission()
    {
        if (empty($this->permission)) {
            return true;
        }
        return Admin::user()->can($this->permission);
    }


}
