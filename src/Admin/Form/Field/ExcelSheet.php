<?php
/**
 * Created by PhpStorm.
 * User: wen
 * Date: 2021/9/5
 * Time: 10:48
 */

namespace WenRuns\Laravel\Admin\Form\Field;


use Encore\Admin\Facades\Admin;
use Encore\Admin\Form\Field;
use WenRuns\Laravel\Laravel;

class ExcelSheet extends Field
{
    protected $view = 'WenAdmin::form.excelsheet';

    protected $showLabel = true;

    protected $uniqueKey = null;

    protected $configs = [
        'rows' => 10,
        'cols' => 10,
        'grid' => [
            'width' => 150,
            'height' => 40,
        ],
        'readonly' => []
    ];

    public function setConfigs($configs)
    {
        $this->configs = $configs;
        return $this;
    }


    public function showLabel($show = true)
    {
        $this->addVariables([
            'showLabel' => $show,
        ]);
        self::setWidth(12, 0);
        return $this;
    }

    public function setWidth($field = 10, $label = 2): Field
    {
        $this->width = [
            'label' => $label,
            'field' => $field,
        ];
        return $this;
    }

    public function getViewElementClasses(): array
    {
        if ($this->horizontal) {
            return [
                'label' => "col-sm-{$this->width['label']} {$this->getLabelClass()}",
                'field' => "col-sm-{$this->width['field']}",
                'form-group' => $this->getGroupClass(true),
            ];
        }

        return ['label' => $this->getLabelClass(), 'field' => '', 'form-group' => ''];
    }

    public function value($value = null)
    {
        if ($value === null) {
            $value = $this->value ?? $this->getDefault();
            return empty($value) ? json_encode([])
                : (is_array($value) ? json_encode($value, JSON_UNESCAPED_UNICODE) : $value);
        }
        $this->value = $value;

        return $this;
    }


    public function variables(): array
    {
        return array_merge($this->variables, [
            'id' => $this->id,
            'name' => $this->elementName ?: $this->formatName($this->column),
            'help' => $this->help,
            'class' => $this->getElementClassString(),
            'value' => $this->value(),
            'label' => $this->label,
            'viewClass' => $this->getViewElementClasses(),
            'column' => $this->column,
            'errorKey' => $this->getErrorKey(),
            'attributes' => $this->formatAttributes(),
            'placeholder' => $this->getPlaceholder(),
            'uniqueKey' => $this->getUniqueKey(),
            'configs' => json_encode($this->configs, JSON_UNESCAPED_UNICODE),
        ]);
    }

    protected function getUniqueKey()
    {
        if (empty($this->uniqueKey)) {
            $this->uniqueKey = mt_rand(100000, 999999);
        }
        return $this->uniqueKey;
    }

    public function render()
    {
        if (!$this->shouldRender()) {
            return '';
        }

        $this->addRequiredAttributeFromRules();

        if ($this->callback instanceof \Closure) {
            $this->value = $this->callback->call($this->form->model(), $this->value, $this);
        }

        Admin::script($this->script);

        Laravel::loadJs('form/excelSheet.js');


        return Admin::component($this->getView(), $this->variables());
    }
}
