<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid\Field;

use WenRuns\Laravel\Admin\Form\Field\Grid\Column;

class Phone extends Column
{
    protected $type = 'phone';

    public function verify($verify = true)
    {
        return $this->config('verify', $verify);
    }

}
