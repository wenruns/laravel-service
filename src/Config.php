<?php


namespace WenRuns\Laravel;

/**
 * Class Config
 * @package WenRuns\Laravel
 * @method static get($key = null, $default = null)
 */
class Config
{
    protected static $instance = null;

    protected $configs = null;


    public function _get($key = null, $default = null)
    {
        $configs = $this->configs;
        if ($configs === null) {
            $configs = $this->configs = config('wenruns_laravel_service');
        }
        if ($key === null) {
            return $configs;
        }
        $key = explode('.', $key);
        foreach ($key as $k) {
            $configs = $configs[$k] ?? null;
            if ($configs === null) {
                return $default;
            }
        }
        return $configs;
    }


    protected static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new static();
        }
        return self::$instance;
    }


    public static function __callStatic($name, $arguments)
    {
        // TODO: Implement __callStatic() method.
        $name = '_'.$name;
        return self::getInstance()->$name(...$arguments);
    }
}
