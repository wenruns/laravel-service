<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid\Field;

use WenRuns\Laravel\Admin\Form\Field\Grid\Column;

class Image extends Column
{
    /**
     * @var string
     */
    protected $type = 'image';


    /**
     * 多选
     * @param $multi
     * @return Image
     */
    public function multiple($multi = true)
    {
        return $this->config('multiple', $multi);
    }

    /**
     * 禁止预览
     * @param $disable
     * @return Image
     */
    public function disabledPreview($disable = true)
    {
        return $this->config('disabledPreview', $disable);
    }


    /**
     * 保存路径
     * @param $path
     * @return Image
     */
    public function savePath($path)
    {
        return $this->config('savePath', $path);
    }

    /**
     * 保存配置
     * @param $disk
     * @return Image
     */
    public function disk($disk)
    {
        return $this->config('disk', $disk);
    }


    /**
     * 上传前回调
     * @param $jsCallback
     * @return Image
     */
    public function beforeUpload($jsCallback)
    {
        if (is_callable($jsCallback)) {
            $jsCallback = call_user_func($jsCallback);
        }
        return $this->config('beforeUpload', $jsCallback);
    }

    /**
     * 上传后回调
     * @param $jsCallback
     * @return Image
     */
    public function afterUpload($jsCallback)
    {
        if (is_callable($jsCallback)) {
            $jsCallback = call_user_func($jsCallback);
        }
        return $this->config('afterUpload', $jsCallback);
    }

    /**
     * 上传url
     * @param $url
     * @return Image
     */
    public function uploadUrl($url)
    {
        return $this->config('url', $url);
    }

    /**
     * 域名
     * @param $host
     * @return Image
     */
    public function host($host)
    {
        return $this->config('host', $host);
    }


    public function beforeRemove($jsCallback)
    {
        if (is_callable($jsCallback)) {
            $jsCallback = call_user_func($jsCallback);
        }
        return $this->config('before_remove', $jsCallback);
    }
}
