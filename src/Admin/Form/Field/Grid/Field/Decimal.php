<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid\Field;

use WenRuns\Laravel\Admin\Form\Field\Grid\Column;

class Decimal extends Column
{
    protected $type = 'decimal';


    public function accuracy($float = 2)
    {
        return $this->config('accuracy', $float);
    }


}
