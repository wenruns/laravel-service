<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid\Field;

use WenRuns\Laravel\Admin\Form\Field\Grid\Column;

class Select extends Column
{
    protected $type = 'select';

    /**
     * 多选
     * @param $multiple
     * @return Select
     */
    public function multiple($multiple = true)
    {
        return $this->config('multiple', $multiple);
    }

    /**
     * 选择选项
     * @param $options
     * @return Select
     */
    public function options($options)
    {
        return $this->config('options', $options);
    }

    /**
     * 异步请求url
     * @param $url
     * @return Select
     */
    public function api($url)
    {
        return $this->config('url', $url);
    }


    /**
     * select2配置选项
     * @param $options
     * @return Select
     */
    public function selectOption($options)
    {
        return $this->setAttribute('selectOption', $options);
    }
}
