<?php
/**
 * Created by PhpStorm.
 * User: Administrator【wenruns】
 * Date: 2021/1/26
 * Time: 18:27
 */

namespace WenRuns\Laravel\Admin\Form\Field\MultiList\Field;


use Encore\Admin\Admin;
use WenRuns\Laravel\Admin\Form\Field\MultiList\Field;

class Display extends Field
{

    public function build()
    {
        // TODO: Implement build() method.
        $class = '';
        if ($this->isRowBool || $this->showLabelBool) {
            $class = 'form-control text';
        }
        return <<<HTML
<div class="{$class} {$this->getClass()} {$this->getColumnClass()}" id="{$this->getClass()}" style="{$this->style}" {$this->buildAttribute()}>{$this->getValue()}</div>
HTML;
    }


    protected function buildEmpty(): string
    {
        $class = '';
        if ($this->isRowBool || $this->showLabelBool) {
            $class = 'form-control text';
        }
        $editClass = 'display-box-edit-'.$this->getUniqueKey();
        $script = <<<SCRIPT
$(function(){
    $(document).on('click', '.{$editClass}', function(e){
        console.log(e.currentTarget.parentElement);
        let ele = e.currentTarget.parentElement.querySelector(".{$editClass}-input");
        ele.style.display = 'block';
        ele.querySelector('input').focus();
    });
    $(document).on('keydown', '.{$editClass}-input input', function(e){
        let parentEle = e.currentTarget.parentElement.parentElement;
        if(e.keyCode == 13){
            let event = e || window.event;
            event.preventDefault();
            parentEle.querySelector(".{$editClass}-content").innerHTML = e.currentTarget.value;
            e.currentTarget.parentElement.style.display = 'none';
        }
    });
    $(document).on('blur', '.{$editClass}-input input', function(e){
        let parentEle = e.currentTarget.parentElement.parentElement;
        parentEle.querySelector(".{$editClass}-content").innerHTML = e.currentTarget.value;
        e.currentTarget.parentElement.style.display = 'none';
    });
});
SCRIPT;
        Admin::script($script);
        // TODO: Implement buildEmpty() method.
        return <<<HTML
<div class="input-group {$class} {$this->getClass()} {$this->getColumnClass()}" style="width: 100%;{$this->style}; position: relative">
    <span class="{$editClass}-content" {$this->buildAttribute()}>{$this->getValue()}</span>&nbsp;&nbsp;<i class="fa fa-edit {$editClass}" style="cursor: pointer"></i>
    <span style="display: none; position: absolute;top: 0px;width: 100%;min-width: 100px;" class="{$editClass}-input">
        <span class="input-group-addon" style="display: {$this->hideIconBool};"><i class="fa fa-pencil fa-fw"></i></span>
        <input type="text" id="{$this->getClass()}" name="{$this->getName()}"  value="{$this->getValue()}" class="form-control text {$this->getColumnClass()} {$this->getClass()}" placeholder="输入{$this->getPlaceholder()}" {$this->buildAttribute()}>
    </span>
</div>
HTML;
    }

}
