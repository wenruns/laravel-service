<?php
/**
 * Created by PhpStorm.
 * User: wen
 * Date: 2021/7/4
 * Time: 15:48
 */

use \WenRuns\Laravel\Button;

if (!function_exists('buttons')) {
    function buttons(array $buttons, \Closure $clusre = null, $toString = true)
    {
        $html = $toString ? '' : [];
        if (isset($buttons[0])) {
            foreach ($buttons as $key => $item) {
                if (is_array($item)) {
                    $button = new Button(\Illuminate\Support\Arr::get($item, 'text'), $item);
                } else {
                    $button = new Button($item);
                }
                if (is_callable($clusre)) {
                    call_user_func($clusre, $button, $key);
                }
                if ($toString) {
                    $html .= $button->render();
                } else {
                    $html[$key] = $button;
                }
            }
        } else {
            $button = new Button(\Illuminate\Support\Arr::get($buttons, 'text'), $buttons);
            if (is_callable($clusre)) {
                call_user_func($clusre, $button, 0);
            }
            if ($toString) {
                $html = $button->render();
            } else {
                $html = $button;
            }
        }
        return $html;
    }
}

if (!function_exists('emptyPage')) {
    function emptyPage($selector = '.table-wrap.table-main')
    {
        \WenRuns\Laravel\Admin\GridService::showEmptyPage($selector);
    }
}


if (!function_exists('isMobile')) {
    function isMobile()
    {
        // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
        if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
            return true;
        }
        //此条摘自TPM智能切换模板引擎，适合TPM开发
        if (isset($_SERVER['HTTP_CLIENT']) && 'PhoneClient' == $_SERVER['HTTP_CLIENT']) {
            return true;
        }
        //如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
        if (isset($_SERVER['HTTP_VIA'])) //找不到为flase,否则为true
        {
            return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
        }
        //判断手机发送的客户端标志,兼容性有待提高
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $clientkeywords = array(
                'nokia', 'sony', 'ericsson', 'mot', 'samsung', 'htc', 'sgh', 'lg', 'sharp', 'sie-', 'philips', 'panasonic', 'alcatel', 'lenovo', 'iphone', 'ipod', 'blackberry', 'meizu', 'android', 'netfront', 'symbian', 'ucweb', 'windowsce', 'palm', 'operamini', 'operamobi', 'openwave', 'nexusone', 'cldc', 'midp', 'wap', 'mobile',
            );
            //从HTTP_USER_AGENT中查找手机浏览器的关键字
            if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
                return true;
            }
        }
        //协议法，因为有可能不准确，放到最后判断
        if (isset($_SERVER['HTTP_ACCEPT'])) {
            // 如果只支持wml并且不支持html那一定是移动设备
            // 如果支持wml和html但是wml在html之前则是移动设备
            if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('compressHtml')) {
    function compressHtml($string)
    {
        return trim(preg_replace(
            ["/[^:]\/\/[^\r\n\t]*[\r\n\t]/", "/> *([^ ]*) *</", "//", "'/\*[^*]*\*/'", "/\r\n/", "/\n/", "/\t/", '/>[ ]+</', '/`/', '/\s+/'],
            [" ", ">\\1<", '', '', '', '', '', '><', '\`', ' '],
            $string
        ));
    }
}
