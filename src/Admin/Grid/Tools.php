<?php

namespace WenRuns\Laravel\Admin\Grid;

use Encore\Admin\Grid\Tools\FilterButton;
use WenRuns\Laravel\Admin\Grid\Tools\BatchActions;

class Tools extends \Encore\Admin\Grid\Tools
{

    protected function appendDefaultTools()
    {
        $this->append(new BatchActions())
            ->append(new FilterButton());
    }

}
