<div class="{{$viewClass['form-group']}} wen-excel-sheet {{$column}} {{$column.$uniqueKey}}">
    @if($showLabel??false)
        :
        <label for="{{$column}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>
    @endif
    <div>
        <div class="{{$viewClass['field']}} sheet-box"></div>
        @include('admin::form.help-block')
    </div>

    <script>
        $(function () {
            new WenRunsExcel({
                $el: '.{{$column.$uniqueKey}} .sheet-box',
                column: '{{$column}}',
                value: {!! $value !!},
                config: {!! $configs !!},
            });
        });
    </script>

</div>
