<?php

namespace WenRuns\Laravel\Admin\Grid;

use Encore\Admin\Admin;

class ColumnSelector extends \Encore\Admin\Grid\Tools\ColumnSelector
{

    public function render()
    {
        if (!$this->grid->showColumnSelector()) {
            return '';
        }

        return Admin::component('WenAdmin::grid.grid-column-selector', [
            'columns' => $this->getGridColumns(),
            'visible' => $this->grid->visibleColumnNames(),
            'defaults' => $this->grid->getDefaultVisibleColumnNames(),
        ]);
    }


}
