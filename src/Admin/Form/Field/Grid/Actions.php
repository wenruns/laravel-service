<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid;

class Actions
{
    /**
     * 屏蔽状态
     * @var bool
     */
    protected $disabled = false;

    /**
     * 新增按钮
     * @var bool
     */
    protected $disableCreateButton = false;

    /**
     * 默认 删除 和 复制 按钮
     * @var array[]
     */
    protected $buttons = [
        [
            'text' => '',
            'icon' => 'fa-trash',
            'type' => 'danger',
            'id' => 'delete',
            'hide' => false,
        ], [
            'text' => '',
            'icon' => 'fa-copy',
            'type' => 'warning',
            'id' => 'copy',
            'hide' => false,
        ]
    ];

    /**
     * 追加自定义按钮
     * @param $buttons
     * @return $this
     */
    public function append($buttons)
    {
        if (isset($buttons[0])) {
            foreach ($buttons as $item) {
                if (is_array($item)) {
                    $this->buttons[] = $item;
                } else {
                    $this->buttons[] = [
                        'text' => $item,
                    ];
                }
            }
        } else {
            if (is_array($buttons)) {
                $this->buttons[] = $buttons;
            } else {
                $this->buttons[] = [
                    'text' => $buttons,
                ];
            }
        }
        return $this;
    }

    /**
     * 隐藏删除按钮
     * @param $value
     * @return $this
     */
    public function disableDelete($value = true)
    {
        $this->buttons[0]['hide'] = $value;
        return $this;
    }

    /**
     * 隐藏复制按钮
     * @param $value
     * @return $this
     */
    public function disableCopy($value = true)
    {
        $this->buttons[1]['hide'] = $value;
        return $this;
    }

    /**
     * 屏蔽新增按钮
     * @param $value
     * @return $this
     */
    public function disableCreateButton($value = true)
    {
        $this->disableCreateButton = $value;
        return $this;
    }

    /**
     * 屏蔽操作
     * @param $value
     * @return $this
     */
    public function disabled($value = true)
    {
        $this->disabled = $value;
        return $this;
    }


    public function render()
    {
        return [
            'disabled' => $this->disabled,
            'buttons' => $this->buttons,
            'disabledCreateButton' => $this->disableCreateButton,
        ];
    }

    public function __get($name)
    {
        return $this->$name;
    }
}
