<div class="box box-info">
    @foreach($form->css() as $item)
        @if($item['type'] == 'link')
            <link rel="stylesheet" href="{{ admin_asset($item['content']) }}"/>
        @else
            <style>{!! $item['content'] !!}</style>
        @endif
    @endforeach
    <div class="box-header with-border">
        <h3 class="box-title">{{ $form->title() }}</h3>

        <div class="box-tools">
            {!! $form->renderTools() !!}
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {!! $form->open() !!}

    <div class="box-body">

        @if(!$tabObj->isEmpty())
            @include('admin::form.tab', compact('tabObj'))
        @else
            <div class="fields-group">

                @if($form->hasRows())
                    @foreach($form->getRows() as $row)
                        {!! $row->render() !!}
                    @endforeach
                @else
                    @foreach($layout->columns() as $column)
                        <div class="col-md-{{ $column->width() }}">
                            @foreach($column->fields() as $field)
                                {!! $field->render() !!}
                            @endforeach
                        </div>
                    @endforeach
                @endif
            </div>
        @endif

    </div>
    <!-- /.box-body -->

    {!! $form->renderFooter() !!}
    @foreach($form->script() as $item)
        @if($item['type'] == 'link')
            <script src="{{admin_asset($item['content'])}}"></script>
        @else
            <script data-exec-on-popstate>{!! $item['content'] !!}</script>
        @endif
    @endforeach
    @foreach($form->getHiddenFields() as $field)
        {!! $field->render() !!}
    @endforeach
    <!-- /.box-footer -->
    {!! $form->close() !!}
</div>


