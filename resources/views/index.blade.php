<div class="plugins--introduce">
    @foreach($introduce as $item)
        @include('WenRuns::introduce',['item'=>$item])
    @endforeach
</div>
<style>

    .plugins--introduce .item.code {
        background: #d2d6de;
        padding: 10px 15px;
    }

    .plugins--introduce .item.text {
        padding: 10px 0px;
    }

    .plugins--introduce .item span.content-line {
        display: block;
        white-space: break-spaces;

    }




</style>
