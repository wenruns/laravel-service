<div class="box grid-box">
    @if(isset($title))
        <div class="box-header with-border">
            <h3 class="box-title"> {{ $title }}</h3>
        </div>
    @endif

    @if ( $grid->showTools() || $grid->showExportBtn() || $grid->showCreateBtn() )
        <div class="wen-grid-box-header">
            <div class="box-header with-border">
                @if ( $grid->showTools() )
                    <div class="pull-left">
                        {!! $grid->renderHeaderTools() !!}
                    </div>
                @endif
                <div class="pull-right">
                    {!! $grid->renderColumnSelector() !!}
                    {!! $grid->renderExportButton() !!}
                    {!! $grid->renderCreateButton() !!}
                </div>
            </div>
        </div>
    @endif

    {!! $grid->renderFilter() !!}

    {!! $grid->renderHeader() !!}

<!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover grid-table" id="{{ $grid->tableID }}">
            <thead>
            <tr>
                @foreach($grid->visibleColumns() as $column)
                    <th {!! $column->formatHtmlAttributes() !!} rowspan="{!! method_exists($column, 'getRowspan')?$column->getRowspan():1 !!}"
                        colspan="{!! method_exists($column, 'getColspan')?$column->getColspan():1 !!}">{!! $column->getLabel() !!}{!! $column->renderHeader() !!}</th>
                @endforeach
            </tr>
            @if($grid instanceof \WenRuns\Laravel\Admin\Grid)
                @foreach($grid->mutators as $k => $columns)
                    <tr>
                        @foreach($columns as $column)
                            <th {!! $column->formatHtmlAttributes() !!} rowspan="{!! $column->getRowspan() !!}"
                                colspan="{!! $column->getColspan() !!}">{!! $column->getLabel() !!}{!! $column->renderHeader() !!}</th>
                        @endforeach
                    </tr>
                @endforeach
            @endif
            </thead>

            @if ($grid->hasQuickCreate())
                {!! $grid->renderQuickCreate() !!}
            @endif

            <tbody>

            @if($grid->rows()->isEmpty() && $grid->showDefineEmptyPage())
                @include('WenAdmin::grid.empty-grid')
            @endif

            @foreach($grid->rows() as $row)
                <tr {!! $row->getRowAttributes() !!}>
                    @foreach($grid->visibleColumnNames() as $name)
                        <td {!! $row->getColumnAttributes($name) !!}>
                            {!! $row->column($name) !!}
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>

            {!! $grid->renderTotalRow() !!}

        </table>

    </div>

    {!! $grid->renderFooter() !!}

    <div class="box-footer clearfix">
        {!! $grid->paginator() !!}
    </div>
    <!-- /.box-body -->
</div>
