<?php


namespace WenRuns\Laravel\Link;


class Node
{
    protected $prev = null;

    protected $next = null;

    protected $value = null;

    public function __construct($value, $prev = null, $next = null)
    {
        $this->prev = $prev;
        $this->next = $next;
        $this->value = $value;
    }

    public function next(Node $node = null)
    {
        if ($node) {
            $this->next = $node;
            $node->prev($this);
        } else {
            return $this->next;
        }
        return $this;
    }

    public function prev(Node $node = null)
    {
        if ($node) {
            $this->prev = $node;
            $node->next($this);
        } else {
            return $this->prev;
        }
        return $this;
    }
}
