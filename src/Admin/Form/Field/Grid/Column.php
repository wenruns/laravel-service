<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid;

class Column
{
    /**
     * 字段类型
     * @var string
     */
    protected $type;
    /**
     * 字段名称
     * @var string
     */
    protected $column;
    /**
     * label
     * @var mixed|string
     */
    protected $label;
    /**
     * 行数据
     * @var null
     */
    protected $row = null;
    /**
     * 格式回调
     * @var null
     */
    public $withClosure = null;

    /**
     * @var null
     */
    protected $default = null;
    /**
     * @var Grid
     */
    protected $grid;
    /**
     * @var array
     */
    protected $attributes = [];

    protected $configs = [];

    /**
     * @param $column
     * @param $label
     */
    public function __construct(Grid $grid, $column, $label = '')
    {
        $this->grid = $grid;
        $this->column = $column;
        $this->label = $label;
        $this->initialization();
    }

    public function style($style)
    {
        return $this->setAttribute('style', $style);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getLabel()
    {
        return $this->label;
    }

    /**
     * 获取字段名称
     * @return string
     */
    public function getName()
    {
        return $this->column;
    }

    protected function initialization()
    {
        $this->configs([
            'error' => '',
            'help' => '',
        ]);
        $this->setAttributes([
            'disabled' => false,
            'readonly' => false,
            'required' => false,
        ]);
        return $this;
    }


    /**
     * 设置默认值
     * @param $default
     * @return $this
     */
    public function default($default)
    {
        $this->default = $default;
        return $this;
    }

    /**
     * 设置行数据
     * @param $row
     * @return $this
     */
    public function setRow($row)
    {
        $this->row = $row;
        return $this;
    }

    /**
     * 设置回调
     * @param \Closure $closure
     * @return $this
     */
    public function with(\Closure $closure)
    {
        $this->withClosure = $closure;
        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
        return $this;
    }

    /**
     * @param array $attrs
     * @return $this
     */
    public function setAttributes(array $attrs)
    {
        $this->attributes = array_merge($this->attributes, $attrs);
        return $this;
    }

    /**
     * @param array $configs
     * @return $this
     */
    public function configs(array $configs)
    {
        $this->configs = array_merge($this->configs, $configs);
        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function config($name, $value)
    {
        $this->configs[$name] = $value;
        return $this;
    }


    /**
     * @param $text
     * @return $this
     */
    public function error($text)
    {
        return $this->config('error', $text);
    }


    /**
     * @param $required
     * @return $this
     */
    public function required($required = true)
    {
        return $this->setAttribute('required', $required);
    }

    /**
     * @param $disable
     * @return $this
     */
    public function disabled($disable = true)
    {
        return $this->setAttribute('disabled', $disable);
    }

    /**
     * @param $text
     * @return $this
     */
    public function help($text)
    {
        return $this->config('help', $text);
    }

    /**
     * @param $readonly
     * @return $this
     */
    public function readonly($readonly = true)
    {
        return $this->setAttribute('readonly', $readonly);
    }

    /**
     * 自定义事件
     * @param $event
     * @param $jsEventCallback
     * @return $this
     */
    public function event($event, $jsEventCallback)
    {
        if (is_callable($jsEventCallback)) {
            $jsEventCallback = call_user_func($jsEventCallback);
        }
        $this->configs['events'][$event][] = compressHtml($jsEventCallback);
        return $this;
    }

    /**
     * @return array
     */
    public function render()
    {
        return [
            'label' => $this->label,
            'name' => $this->column,
            'type' => $this->type,
            'default' => $this->default,
            'attributes' => $this->attributes,
            'configs' => $this->configs,
        ];
    }

    /**
     *
     * @return null
     */
    public function getRow()
    {
        $this->row[$this->column] = $this->getValue();
        return $this->row;
    }

    /**
     * 获取行值
     * @return mixed|null
     */
    public function getValue()
    {
        $value = $this->row[$this->column] ?? null;
        $value = empty($value) && $value !== 0 ? $this->default : $value;
        if ($this->withClosure instanceof \Closure) {
            $value = $this->withClosure->call($this, $value);
        }
        return $value;
    }

    /**
     * 设置宽度
     * @param $width
     * @param $labelWidth
     * @return $this
     */
    public function width($width = 16, $labelWidth = 8)
    {
        return $this->configs([
            'width' => $width,
            'labelWidth' => $labelWidth,
        ]);
    }

    /**
     * 是否启用排序
     * @param $sort
     * @return $this
     */
    public function sortable($sort = true)
    {
        return $this->config('sortable', $sort);
    }

    /**
     * 筛选
     * @param $type
     * @param $options
     * @return $this
     */
    public function filter($type = 'like', $options = [])
    {
        return $this->config('filter', [
            'type' => $type,
            'options' => $options,
        ]);
    }


    /**
     * @param $key
     * @param $default
     * @return array|mixed|null
     */
    public function getConfigs($key = null, $default = null)
    {
        if (empty($key)) {
            return $this->configs;
        }
        $configs = $this->configs;
        $key = explode('.', $key);
        foreach ($key as $k) {
            if (empty($k)) {
                continue;
            }
            $configs = $configs[$k] ?? $default;
        }
        return $configs;
    }

    /**
     * @param bool $disable
     * @return $this
     */
    public function disableLabel(bool $disable = true)
    {
        return $this->config('disabledLabel', $disable);
    }


    /**
     * @param $value
     * @return $this
     */
    public function placeholder($value)
    {
        return $this->setAttribute('placeholder', $value);
    }

    /**
     * 获取行属性
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->$name ?? null;
    }

}
