<?php


namespace WenRuns\Laravel\Admin\Grid;

use Illuminate\Database\Query\Expression;
use Str;



class Model extends \Encore\Admin\Grid\Model
{

    public function getQueryBuilder()
    {
        if ($this->relation) {
            return $this->relation->getQuery();
        }

        $this->setSort();

        $queryBuilder = $this->originalModel;

        $this->queries->reject(function ($query) {
            return in_array($query['method'], ['get', 'paginate']);
        })->each(function ($query) use (&$queryBuilder) {
            $queryBuilder = $queryBuilder->{$query['method']}(...$query['arguments']);
        });

        return $queryBuilder;
    }

    /**
     * Summary of getGrid
     * @return \WenRuns\Laravel\Admin\Grid
     */
    public function getGrid()
    {
        return $this->grid;
    }

    /**
     * Set the grid sort.
     *
     * @return void
     */
    protected function setSort()
    {
        $this->sort = \request($this->sortName, []);
        if (!is_array($this->sort)) {
            return;
        }

        $columnName = $this->sort['column'] ?? null;
        if ($columnName === null || empty($this->sort['type'])) {
            return;
        }

        if ($sorter = $this->getGrid()->sorters[$this->sort['column']] ?? null) {
            if (is_callable($sorter->callback)) {
                $this->resetOrderBy();
                \Closure::fromCallable($sorter->callback)->call($this, $this->sort['type']);
                return;
            }
        }

        $columnNameContainsDots = Str::contains($columnName, '.');
        $isRelation = $this->queries->contains(function ($query) use ($columnName) {
            // relationship should be camel case
            $columnName = Str::camel(Str::before($columnName, '.'));

            return $query['method'] === 'with' && in_array($columnName, $query['arguments'], true);
        });
        if ($columnNameContainsDots === true && $isRelation) {
            $this->setRelationSort($columnName);
        } else {
            $this->resetOrderBy();

            if ($columnNameContainsDots === true) {
                //json
                $this->resetOrderBy();
                $explodedCols = explode('.', $this->sort['column']);
                $col = array_shift($explodedCols);
                $parts = implode('.', $explodedCols);
                $columnName = "JSON_EXTRACT({$col}, '$.{$parts}')";
            }

            // get column. if contains "cast", set set column as cast
            if (!empty($this->sort['cast'])) {
                $column = "CAST({$columnName} AS {$this->sort['cast']}) {$this->sort['type']}";
                $method = 'orderByRaw';
                $arguments = [$column];
            } else {
                $column = $columnNameContainsDots ? new Expression($columnName) : $columnName;
                $method = 'orderBy';
                $arguments = [$column, $this->sort['type']];
            }

            $this->queries->push([
                'method' => $method,
                'arguments' => $arguments,
            ]);
        }
    }

}
