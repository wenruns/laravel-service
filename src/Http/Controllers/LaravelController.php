<?php

namespace WenRuns\Laravel\Http\Controllers;

use App\Admin\Services\Expands\Test;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;

class LaravelController extends Controller
{
    /**
     * 组件介绍
     * @param Content $content
     * @return Content
     */
    public function index()
    {
        return view('WenRuns::index', [
            'introduce' => [
                [
                    'type' => 'h1',
                    'title' => '一、安装',
                    'content' => [
                        [
                            'type' => 'code',
                            'content' => 'composer require wenruns/laravel-service',
                        ]
                    ],
                ], [
                    'type' => 'h1',
                    'title' => '二、发布静态资源',
                    'content' => [
                        [
                            'type' => 'code',
                            'content' => 'php artisan vendor:publish --provider=WenRuns\Laravel\LaravelServiceProvider'
                        ]
                    ]
                ], [
                    'type' => 'h1',
                    'title' => '三、功能介绍',
                    'content' => [
                        [
                            'type' => 'h2',
                            'title' => '1、GridService服务',
                            'content' => [
                                [
                                    'type' => 'text',
                                    'content' => '该服务二次封装的Grid的使用，美化代码视图。例如：'
                                ], [
                                    'type' => 'code',
                                    'content' => '$grid = GridService::instance(ModelClass)
    ->where(function(Grid/Model $model) {})
    ->content(function(Grid $grid){})
    ->tools(function(Tools $tools){})
    ->actions(function(Actions $actions){})
    ->statistic(["consume","number"]) // 统计当页指定字段
    ->mergeColspan(["id"]) // 合并相同的值
    ->exporter(new Exporter::class)
    ->disableCreateButton()
    ->disableFilter()
    ->disableBatchActions()
    ->render()',
                                ]
                            ]
                        ], [
                            'type' => 'h2',
                            'title' => '2、FormService服务',
                            'content' => [
                                [
                                    'type' => 'text',
                                    'content' => '该服务二次封装Form表单的使用，美化代码试图。例如：',
                                ], [
                                    'type' => 'code',
                                    'content' => '$form = FormService::instance(Model::class)
    ->content(function(Form $form){})
    ->saving(Form $form){}
    ->render();'
                                ]
                            ]
                        ], [
                            'type' => 'h3',
                            'title' => '3、更多功能服务',
                            'content' => [
//                                [
//                                    'type'=>'',
//                                    'content'=>'',
//                                ]
                            ]
                        ]
                    ]
                ]
            ],

        ]);
    }

    /**
     * formGrid组件的modal | expand内容
     * @param Request $request
     * @return mixed
     * @throws \ReflectionException
     */
    public function formGrid(Request $request)
    {
        $class = $request->input('__class__');
        if ($class && class_exists($class)) {
            $instance = new \ReflectionClass($class);
            return $instance->newInstance()->render($request->input('__key__'));
        }
        throw new \Exception($class . ' not exist.');
    }


    /**
     * formGrid组件上传
     * @param Request $request
     * @return string
     */
    public function formGridUpload(Request $request)
    {
        $file = $request->file('file');
        $path = $request->input('path');
        if (empty($path)) {
            $path = date('Ymd');
        }
        $disk = $request->input('disk');
        $options = ['disk' => config('admin.upload.disk', 'admin')];
        if ($disk) {
            $options['disk'] = $disk;
        }
        if ($file) {
            $res = $file->storeAs($path, md5($file->getClientOriginalName()) . time() . '.' . $file->getClientOriginalExtension(), $options);
            $url = config('filesystems.disks.' . $options['disk'] . '.url');
            return trim($url, '/') . '/' . $res;
        }
        if ($url = $request->input('url')) {
            $uri = str_replace(config('filesystems.disks.' . $options['disk'] . '.url'), '', $url);
            $root = config('filesystems.disks.' . $options['disk'] . '.root', public_path());
            if (is_file($root . DIRECTORY_SEPARATOR . $uri)) {
                unlink($root . DIRECTORY_SEPARATOR . $uri);
            }
            return Response::json([
                'status' => true,
                'message' => 'ok',
            ]);
        }
    }

}
