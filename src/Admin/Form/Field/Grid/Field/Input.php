<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid\Field;

use WenRuns\Laravel\Admin\Form\Field\Grid\Column;

class Input extends Column
{
    protected $type = 'input';


    public function type($type)
    {
        return $this->setAttribute('type', $type);
    }
}
