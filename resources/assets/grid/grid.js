const mergeColspan = function ({
                                   tableSelector,
                                   mergeInput = 0,
                                   columns = [],
                                   fn = false,
                               }) {
    this.tableBody = document.querySelector(tableSelector + " tbody");
    this.fixedTableBody = this.tableBody.parentElement.parentElement.parentElement.querySelector('.table-fixed-right tbody');
    this.prevTDs = {};
    this.merge = {};
    this.prevTrIndex = {};
    this.prevForms = {};
    this.inputElements = {};
    this.mergeInput = mergeInput;
    this.columns = columns;
    this.fn = fn;

    this.Trim = function (str) {
        return str.replace(/(^\s*)|(\s*$)/g, "");
    }
    this.isComplexEle = (ele) => {
        if (ele.inputmask) {
            return true;
        }
        if (ele.tagName == 'SELECT') {
            return true;
        }
        if (Array.from(ele.classList).indexOf('initialized') >= 0) {
            return true;
        }
        return false;
    }
    this.checkInput = (ele, j, i) => {
        if (ele.children.length) {
            Array.from(ele.children).forEach((ele, k) => {
                let name = ele.getAttribute("name");
                if (name && ['_token', '_method', '_previous_'].indexOf(name) < 0) {
                    if (!this.inputElements[i]) {
                        this.inputElements[i] = {};
                    }
                    this.inputElements[i][name] = ele;
                    if (this.merge[i]) {
                        if (this.mergeInput) {
                            // 方式1合并，只是组合各项value，用英文逗号隔开
                            if (this.mergeInput == 1) {
                                let prevEle = this.inputElements[this.prevTrIndex[j]][name];
                                let prevValue = prevEle.value.split(',');
                                prevValue.push(ele.value);
                                prevEle.value = prevValue.join(',');
                            }
                            // 方式2合并，去重复并且过滤空值合并各项的值
                            else if (this.mergeInput == 2 && ele.value) {
                                let prevEle = this.inputElements[this.prevTrIndex[j]][name];
                                let prevValue = prevEle.value.split(',').filter(function (item) {
                                    return item;
                                });
                                if (prevValue.indexOf(ele.value) < 0) {
                                    prevValue.push(ele.value);
                                    prevEle.value = prevValue.join(',');
                                }
                            }
                        } else { // 不启用合并方式，只追加输入框的形式
                            ele.setAttribute('name', name + '[]');
                            let preEle = this.inputElements[this.prevTrIndex[j]][name];
                            preEle.setAttribute('name', name + '[]');
                            if (this.isComplexEle(ele)) {
                                if (ele.tagName == 'SELECT') {
                                    let parentEle = preEle.parentElement;
                                    if (parentEle.classList.value.indexOf('merge-input') < 0) {
                                        let parentDom = document.createElement('div');
                                        parentDom.classList.value = parentEle.classList.value;
                                        parentEle.classList.add('merge-input')
                                        parentEle.parentElement.append(parentDom);
                                        parentDom.append(parentEle);
                                        parentEle.classList.value = '';
                                    }
                                    ele.parentElement.classList.value = '';
                                    $(ele.parentElement).insertAfter(preEle.parentElement);
                                } else {
                                    $(ele.parentElement).insertAfter(preEle.parentElement);
                                }
                            } else {
                                $(ele).insertAfter(preEle);
                            }
                        }
                    }
                } else if (ele.children.length) {
                    if (ele.tagName == 'FORM' && !this.prevForms[this.prevTrIndex[j]]) {
                        this.prevForms[this.prevTrIndex[j]] = ele;
                    }
                    this.checkInput(ele, j, i);
                }
            });
        }
    };

    Array.from(this.tableBody.children).forEach((ele, i) => {
        if (this.fn) {
            this.merge[i] = this.fn.call(this, ele, i);
        }
        Array.from(ele.children).forEach((td, j) => {
            if (!td.style['border-right']) {
                td.style['border-right'] = '1px solid #f4f4f4';
            }
            if (this.columns[0] != '*') {
                let classColumn = td.classList.value.replace('column-', '');
                if (this.columns.indexOf(classColumn) < 0) {
                    return false;
                }
            }
            let prevTd = this.prevTDs[j];
            if (!this.fn) {
                this.merge[i] = prevTd && this.Trim(prevTd.innerHTML) == this.Trim(td.innerHTML);
            }
            if (this.merge[i]) {
                let rowspan = Number(prevTd.getAttribute("rowspan"));
                prevTd.setAttribute('rowspan', rowspan ? (rowspan + 1) : 2);
                prevTd.style['vertical-align'] = 'middle';
                td.remove();
            } else {
                this.prevTDs[j] = td;
                this.prevTrIndex[j] = i;
            }
            this.checkInput(td, j, i);
        });
        if (this.fixedTableBody) {
            if (!this.resizeObserver) {
                this.resizeObserver = {};
            }
            if (typeof ResizeObserver == 'undefined') {
                console.warn('由于无法监控表格高度变化，请不要使用grid->fixedColumn()方法，否则可能会出现页面位置错乱！！！');
            } else {
                this.resizeObserver[i] = new ResizeObserver(entries => {
                    let h = entries[0].contentRect.height;
                    this.fixedTableBody.children[i].style.height = h + 'px';
                });
                this.resizeObserver[i].observe(ele);
            }
        }
    });
}


const FloatScrollbar = function ({
                                     el = '.table-wrap.table-main',
                                     z_index = 11,
                                 } = {}) {
    this.id = 'float-scrollbar-' + Date.now()
    this.$scrollbar = null;
    this.z_index = z_index;
    if (typeof el == "string") {
        this.$gridEl = document.querySelector(el);
    } else {
        this.$gridEl = el;
    }
    this.mouse = {
        down: false,
        x: 0,
    }
    this.percent = 1;
    this.checkScrollbar();
}

FloatScrollbar.prototype = {
    $el: document,
    $gridEl: null,
    $scrollbar: null,
    mouse: {
        down: false,
        x: 0,
    },
    percent: 1,
    z_index: 1,
    checkScrollbar() {
        if (this.$gridEl && this.isScroll(this.$gridEl).scrollX && this.isScroll().scrollY) {
            this.createScrollbar();
            this.showScrollbar();
            this.$el.addEventListener('scroll', (e) => {
                this.documentScrollEvent(e);
            });
            this.$gridEl.addEventListener('scroll', (e) => {
                this.gridScrollEvent(e);
            });
        } else {
            this.hideScrollbar();
        }
    },
    createStyle() {
        let style = document.createElement('style');
        let width = 500 * this.$gridEl.clientWidth / this.$gridEl.scrollWidth;
        this.percent = (500 - width) / (this.$gridEl.scrollWidth - this.$gridEl.clientWidth);
        style.innerHTML = `
            .float-scrollbar{
                position: fixed;
                z-index: ${this.z_index};
                bottom: 10px;
                right: 20px;
                width: 500px;
                height: 15px;
                border-radius: 10px;
                overflow: hidden;
                background: rgba(0, 0, 0, 0.2);
                display: flex;
                align-items: center;
            }
            .float-scrollbar .scrollbar-tools{
                height:calc(100% - 2px);
                width: ${width}px;
                background: rgba(110, 112 ,40, .5);
                border-radius: 10px;
                cursor: pointer;
            }
        `;
        document.head.append(style);
        return this;
    },
    gridScrollEvent(e) {
        let el = this.$scrollbar.querySelector('.scrollbar-tools');
        if (el) {
            let left = 500 * e.currentTarget.scrollLeft / e.currentTarget.scrollWidth;
            el.style.transform = 'translateX(' + left + 'px)';
        }
    },
    createScrollbar() {
        this.createStyle();
        let scrollbar = document.createElement('div');
        scrollbar.classList.add('float-scrollbar');
        scrollbar.setAttribute('id', this.id);
        scrollbar.innerHTML = `<div class="scrollbar-tools"></div>`;
        this.$scrollbar = scrollbar;
        document.body.append(this.$scrollbar);
        return this.scrollbarEvent();
    },
    scrollbarEvent() {
        let el = this.$scrollbar.querySelector('.scrollbar-tools');
        if (el) {
            el.addEventListener('mousedown', (e) => {
                this.mouse.down = true;
                this.mouse.x = e.pageX || e.x || e.clientX;
                document.body.setAttribute('onselectstart', 'return false')
            });
            document.body.addEventListener('mousemove', (e) => {
                if (this.mouse.down) {
                    let x = e.pageX || e.x || e.clientX;
                    let _x = x - this.mouse.x;
                    this.mouse.x = x;
                    let left = (_x / this.percent) + this.$gridEl.scrollLeft;
                    this.$gridEl.scrollTo(left, 0);
                }
            });
            document.body.addEventListener('mouseup', (e) => {
                this.mouse.down = false;
                this.mouse.x = 0;
                document.body.removeAttribute('onselectstart');
            })
        }
    },
    showScrollbar() {
        let eles = document.querySelectorAll('.float-scrollbar');
        eles.forEach((el) => {
            el.style.visibility = 'hidden';
        });
        if (this.$scrollbar) {
            this.$scrollbar.style.visibility = 'visible';
        }
        return this;
    },
    hideScrollbar() {
        let eles = document.querySelectorAll('.float-scrollbar');
        eles.forEach((el) => {
            el.style.visibility = 'hidden';
        });
        if (this.$scrollbar) {
            this.$scrollbar.style.visibility = 'hidden';
        }
        return this;
    },
    documentScrollEvent(e) {
        let clientHeight = document.documentElement.clientHeight;
        let rect = this.$gridEl.getBoundingClientRect();
        if (clientHeight >= rect.bottom) {
            this.hideScrollbar();
        } else {
            this.showScrollbar();
        }
    },

    isScroll(el) {
        // test targets
        var elems = el ? [el] : [document.documentElement, document.body];
        var scrollX = false, scrollY = false;
        for (var i = 0; i < elems.length; i++) {
            var o = elems[i];
            // test horizontal
            var sl = o.scrollLeft;
            o.scrollLeft += (sl > 0) ? -1 : 1;
            o.scrollLeft !== sl && (scrollX = scrollX || true);
            o.scrollLeft = sl;
            // test vertical
            var st = o.scrollTop;
            o.scrollTop += (st > 0) ? -1 : 1;
            o.scrollTop !== st && (scrollY = scrollY || true);
            o.scrollTop = st;
        }
        // ret
        return {
            scrollX: scrollX,
            scrollY: scrollY
        };
    },
}
