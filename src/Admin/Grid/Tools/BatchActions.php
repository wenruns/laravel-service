<?php

namespace WenRuns\Laravel\Admin\Grid\Tools;

use Encore\Admin\Admin;

class BatchActions extends \Encore\Admin\Grid\Tools\BatchActions
{
    protected $holdAll = false;

    public function render()
    {
        if (!$this->enableDelete) {
            $this->actions->shift();
        }

        $this->addActionScripts();

        return Admin::component('WenAdmin::grid.batch-actions', [
            'all' => $this->grid->getSelectAllName(),
            'row' => $this->grid->getGridRowName(),
            'actions' => $this->actions,
            'holdAll' => $this->holdAll,
        ]);
    }
}
