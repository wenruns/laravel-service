<?php

namespace WenRuns\Laravel\Admin\Form;

class Builder extends \Encore\Admin\Form\Builder
{
    protected $view = 'WenAdmin::form';


    public function css()
    {
        return $this->form->css();
    }

    public function script()
    {
        return $this->form->script();
    }
}
