<?php

namespace WenRuns\Laravel\Admin\Form\Field\Grid\Field;

use WenRuns\Laravel\Admin\Form\Field\Grid\Column;

class Textarea extends Column
{
    protected $type = 'textarea';

    public function rows($rows)
    {
        return $this->setAttribute('rows', $rows);
    }
}
